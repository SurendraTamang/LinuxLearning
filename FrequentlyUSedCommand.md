# Here are the list of the commands which are frequently used in the form of table

|Command|Its Functions|Argument
|:-------|:------------|:--------
|cal|to show calender| month year
|cp|to copy the files |cp (source) (destination), -r for whole folder
|cd|to change the directory| cd Downloads,cd ~ --> to home cd - previous directory
|curl|Downloading files from the web|
|echo|print the file or it function|
|ls|Helps to list the files and folder| -lh,-a,l
|pwd|print working directory|
|mv |moving the directory the | mv (source) (directory) it can be used to rename the file
|which|it will show the path of the script|
|rm|to delete the file|-r for whole or use rmdir,rm -rf (very danger)
|time|to show time|
