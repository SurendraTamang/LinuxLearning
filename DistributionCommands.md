# The commands for seeing the system information.
Command|Function| Other uses
--------|:-------|:----------
uname|to see the kernel version and distribution| -r -->only kernel version -a -->Shows all
whereis|to see the sources|whereis firefox
