# Terms we use frequently in the Linux Environment.
1. Kernel

It is the brain of the Linux Operating System. It controls the hardware and interact with the application. It is the glue between Hardware and Application
![](./img/Kernel_Layout.svg.png)

2. Distribution

A collection of the program combined with the linux kernel to make up Linux based OS.Some of them are RHEL,Fedora,Ubuntu etc.It is also known as Distro.
![](./img/choosing-a-linux-distro.jpg)

3. Boot Loader 

A small program that boots the OS.GRUB,ISOlinux are some example.

![](./img/bootloader.jpg)

The term boot is used as the process taken by the computer when turned on that loads the OS and prepare the system for use.

4. Service

Program that runs as the background process.E.g httpd,ftpd,named,dchpd etc
![](./img/Service.png)
5. File System 

The method of organizing and storing the files in the linux system in linux.Eg.EXT3,EXT4,FAT,XFS,NTFS,Btrfs
![](./img/filesystem.png)
6. X Window System

It is the standard toolkit which provide the GUI to all Linux System.
![](./img/xwindowsystem.png)

7. Desktop Environment

GUI on the top of the OS.Eg GNOME,Fluxbox,KDE etc
![](./img/desktopenvironment.png)
8. Command Line

an interface for typing commands directly to a OS.
![](./img/cli.png)
9. Shell
Command line interpreter that interprets the command line input and instruct the OS to perform any necessary task and command eg bash,tcsh
	* It is an environment where we can run commands,programs and shell script
![](./img/shell.png)
 

